﻿using UnityEngine;

// Static class otside of the game logic
public static class AppHelper
{
    public static void Quit() {
#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
#else
        Application.Quit();
#endif
    }
}
