﻿using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;

[RequireComponent(typeof(Text))]
public class ObjectiveTextController : MonoBehaviour
{
    private Text _text;

    public string Text {
        get {
            return _text.text;
        }
        set {
            _text.text = value;
        }
    }

    private void Awake() {
        _text = GetComponent<Text>();
    }

    private void Start() {
        FadeIn();
    }

    public void FadeIn() {
        _text.DOFade(0, 0);
        _text.DOFade(1, 1);
    }

    public void FadeOut() {
        _text.DOFade(0, 1);
    }
}
