﻿using System;
using UnityEngine.Events;

[Serializable]
public class CardPickedEvent : UnityEvent<CardController>
{
}
