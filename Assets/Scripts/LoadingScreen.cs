﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using DG.Tweening;

[RequireComponent(typeof(Image))]
public class LoadingScreen : GameScreen
{
    [SerializeField]
    private Text _loadingText;

    [SerializeField]
    private UnityEvent _loadEvent;

    [SerializeField]
    private UnityEvent _loadEndedEvent;

    private void Update() {
        if (_loadingText != null) {
            _loadingText.DOFade(_background.color.a, 0);
        }
    }

    public void StartLoading() {
        FadeIn(OnScreenFadedIn);
    }

    private void OnScreenFadedIn() {
        _loadEvent.Invoke();
        FadeOut(OnScreenFadedOut);
    }

    private void OnScreenFadedOut() {
        _loadEndedEvent.Invoke();
    }
}
