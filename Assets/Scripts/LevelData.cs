﻿using System;
using UnityEngine;

[Serializable]
public class LevelData
{
    [SerializeField]
    private Vector2Int _gridSize;

    public Vector2Int GridSize => _gridSize;

    public int RequiredCardsCount => _gridSize.x * _gridSize.y;
}
