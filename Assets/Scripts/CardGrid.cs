﻿using UnityEngine;

public class CardGrid
{
    public GameObject[,] Data { get; private set; }

    public CardGrid(Vector2Int gridSize) {
        Data = new GameObject[gridSize.x, gridSize.y];
    }

    public Vector2Int GetSize() {
        return new Vector2Int(Data.GetLength(0), Data.GetLength(1));
    }

    public void Clear() {
        if (Data != null) {
            foreach (GameObject card in Data) {
                Object.Destroy(card);
            }
        }

        Data = null;
    }
}
