﻿using UnityEngine;

public class CardGridBuilder
{
    private BuilderCardBundle[] _cardBundles;

    public CardGridBuilder(CardBundleData[] cardBundles) {
        _cardBundles = new BuilderCardBundle[cardBundles.Length];
        for (int i = 0; i < cardBundles.Length; ++i) {
            _cardBundles[i] = new BuilderCardBundle(cardBundles[i]);
        }
    }

    public CardGrid Build (LevelData levelData, GameObject cardPrefab, out CardData objectiveCard) {
        BuilderCardBundle randomCardBungle = GetRandomCardBundle();
        if (levelData.RequiredCardsCount > randomCardBungle.CardsCount) {
            Debug.LogError("Fatal error: not enough unique cards to build a valid level.");
            AppHelper.Quit();
        }

        CardData[] shuffledCards = randomCardBungle.GetShuffledCards();
        objectiveCard = randomCardBungle.GetNextUniqueCard();

        // Move objective card forward to make sure it will be in generated pool.
        for (int i = levelData.RequiredCardsCount; i < shuffledCards.Length; ++i) {
            if (shuffledCards[i] == objectiveCard) {
                int swapIndex = Random.Range(0, levelData.RequiredCardsCount);
                CardData temp = shuffledCards[i];
                shuffledCards[i] = shuffledCards[swapIndex];
                shuffledCards[swapIndex] = temp;
                break;
            }
        }

        Vector2Int gridSize = levelData.GridSize;

        float cardSize = cardPrefab.transform.localScale.y; // Presuming card is quadratic.
        Vector3 spawnCenter = new Vector3(-cardSize * (gridSize.x / 2f - 0.5f), -cardSize * (gridSize.y / 2f - 0.5f), 0);

        CardGrid cardGrid = new CardGrid(levelData.GridSize);

        int cardIndex = 0;
        for (int y = 0; y < gridSize.y; ++y) {
            for (int x = 0; x < gridSize.x; ++x) {
                Vector3 spawnPoint = new Vector3(spawnCenter.x + cardSize * x, spawnCenter.y + cardSize * y, spawnCenter.z);
                GameObject newCard = GameObject.Instantiate(cardPrefab, spawnPoint, Quaternion.identity);
                newCard.GetComponent<CardController>().CardData = shuffledCards[cardIndex];
                cardGrid.Data[x, y] = newCard;
                ++cardIndex;
            }
        }

        return cardGrid;
    }

    
    private BuilderCardBundle GetRandomCardBundle() {
        // Picking random card bundle that haven't yet run out of session's unique cards.
        int startIndex = Random.Range(0, _cardBundles.Length);
        int index = startIndex;
        while (++index != startIndex) {
            if (index >= _cardBundles.Length) {
                index = 0;
            }
            if (!_cardBundles[index].IsUniqueSequenceEnded()) {
                return _cardBundles[index];
            }
        }

        // No more unique cards. Shuffle and reset.
        foreach (BuilderCardBundle cardBundle in _cardBundles) {
            cardBundle.ResetUniqueSequence();
        }
        return _cardBundles[Random.Range(0, _cardBundles.Length)];
    }

    /* Helper class for builder because it needs additional logic in order to generate correct levels 
     * where each next card grid contains a new card not shown before. */
    private class BuilderCardBundle
    {
        private CardBundleData _cardBundleData;
        private CardData[]     _shuffledUniqueCardSequence;
        private int            _nextUniqueCardIndex;

        public int CardsCount => _cardBundleData.CardData.Length;

        public BuilderCardBundle(CardBundleData cardBundleData) {
            _cardBundleData = cardBundleData;
            _shuffledUniqueCardSequence = GetShuffledCards();
            _nextUniqueCardIndex = 0;
        }

        public CardData[] GetShuffledCards() {
            CardData[] shuffledCards = _cardBundleData.CardData.Clone() as CardData[];

            int i = shuffledCards.Length;
            while (i > 1) {
                int randomIndex = Random.Range(0, i--);

                CardData temp = shuffledCards[randomIndex];
                shuffledCards[randomIndex] = shuffledCards[i];
                shuffledCards[i] = temp;
            }

            return shuffledCards;
        }

        public CardData GetNextUniqueCard() {
            if (_nextUniqueCardIndex >= _shuffledUniqueCardSequence.Length) {
                _nextUniqueCardIndex = 0;
            }
            return _shuffledUniqueCardSequence[_nextUniqueCardIndex++];
        }

        public bool IsUniqueSequenceEnded() {
            return _nextUniqueCardIndex >= _shuffledUniqueCardSequence.Length;
        }

        public void ResetUniqueSequence() {
            _shuffledUniqueCardSequence = GetShuffledCards();
            _nextUniqueCardIndex = 0;
        }
    }
}
