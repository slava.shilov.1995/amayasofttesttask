﻿using UnityEngine;
using UnityEngine.Events;

public class ProgressManager : MonoBehaviour
{
    [SerializeField]
    private LevelBundleData _levelBundle;

    [SerializeField]
    private UnityEvent _gameCompletedEvent;

    private UnityEvent _levelCompletedEvent;

    private string _objectiveCardIdentifier;

    private PlayGridController _playGridController;
    private InputHandler _inputHandler;
    private ObjectiveTextController _objectiveText;

    private int _currentLevel;

    private void Awake() {
        _playGridController = GameObject.Find("PlayGrid")?.GetComponent<PlayGridController>();
        if (!_playGridController) {
            Debug.LogError("Fatal error: PlayGridController not found.");
            AppHelper.Quit();
        }

        _levelCompletedEvent = new UnityEvent();
        _levelCompletedEvent.AddListener(OnLevelCompleted);

        _inputHandler = GameObject.Find("InputHandler")?.GetComponent<InputHandler>();
        if (!_playGridController) {
            Debug.LogError("Fatal error: InputHandler not found.");
            AppHelper.Quit();
        }

        _objectiveText = GameObject.Find("ObjectiveText")?.GetComponent<ObjectiveTextController>();
        if (!_objectiveText) {
            Debug.LogError("Fatal error: Objective text field not found.");
            AppHelper.Quit();
        }

        _currentLevel = 0;
    }

    private void Start() {
        StartLevel();
    }

    public void StartLevel() {
        CardData objectiveCard;
        _playGridController.FillGrid(_levelBundle.LevelData[_currentLevel], _currentLevel == 0, out objectiveCard);

        _objectiveCardIdentifier = objectiveCard.Identifier;
        _objectiveText.Text = "Find " + _objectiveCardIdentifier;
    }

    public void ResetProgress() {
        _currentLevel = 0;
        _playGridController.ClearGrid();
    }

    private void OnLevelCompleted() {
        if (_currentLevel + 1 >= _levelBundle.LevelData.Length) {
            _gameCompletedEvent.Invoke();
        } else {
            ++_currentLevel;
            StartLevel();
        }
    }

    public void CheckAnswer(CardController pickedCard) {
        if (pickedCard.CardData.Identifier == _objectiveCardIdentifier) {
            pickedCard.DoSuccessAnimation(() => _levelCompletedEvent.Invoke());
        } else {
            pickedCard.DoWrongAnimation();
        }
    }
}
