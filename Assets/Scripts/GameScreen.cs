﻿using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

[RequireComponent(typeof(Image))]
[DisallowMultipleComponent()]
public class GameScreen : MonoBehaviour
{
    protected Image _background;

    private float _initialFadeValue;

    private Sequence _animationSequence;

    private void Awake() {
        _background = GetComponent<Image>();

        _initialFadeValue = _background.color.a;
        _background.DOFade(0, 0);
    }

    private void Start() {
        gameObject.SetActive(false);
    }

    public void FadeIn(TweenCallback callback = null) {
        gameObject.SetActive(true);

        _animationSequence?.Kill();
        
        _animationSequence = DOTween.Sequence();
        _animationSequence.Append(_background.DOFade(0, 0))
                          .Append(_background.DOFade(_initialFadeValue, 1))
                          .OnComplete(callback);
    }

    // Full function width default argument is not available in Inspector.
    public void FadeIn() {
        FadeIn(null);
    }

    public void FadeOut(TweenCallback callback = null) {
        _background.DOFade(0, 1).OnComplete(() => {
            if (callback != null) {
                callback.Invoke();
            }
            gameObject.SetActive(false);
        });
    }

    public void FadeOut() {
        FadeOut(null);
    }
}
