﻿using UnityEngine;

public class InputHandler : MonoBehaviour
{
    [SerializeField]
    public CardPickedEvent _cardPickedEvent;

    private ProgressManager _progressManager;

    private void Awake() {
        _progressManager = GameObject.Find("ProgressManager")?.GetComponent<ProgressManager>();
        if (!_progressManager) {
            Debug.LogError("Fatal error: ProgressManager not found.");
            AppHelper.Quit();
        }
    }

    private void Update() {
        if (Input.GetMouseButtonDown(0)) {
            RaycastHit2D hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector2.zero);

            if (hit.collider != null) {
                CardController cardController = hit.transform.gameObject.GetComponent<CardController>();
                if (cardController) {
                    _cardPickedEvent.Invoke(cardController);
                }
            }
        }
    }
}
