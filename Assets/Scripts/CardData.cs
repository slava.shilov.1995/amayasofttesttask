﻿using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class CardData
{
    [SerializeField]
    private string _identifier;

    [SerializeField]
    private Sprite _sprite;

    [SerializeField]
    private bool _isSpriteRotated;

    public string Identifier => _identifier;

    public Sprite Sprite => _sprite;

    public bool IsSpriteRotated => _isSpriteRotated;

    public override bool Equals(object obj) {
        var data = obj as CardData;
        return data != null && _identifier == data._identifier;
    }

    public override int GetHashCode() {
        return 1278429765 + EqualityComparer<string>.Default.GetHashCode(_identifier);
    }

    public static bool operator == (CardData card1, CardData card2) {
        return card1._identifier == card2._identifier;
    }

    public static bool operator !=(CardData card1, CardData card2) {
        return card1._identifier != card2._identifier;
    }
}
