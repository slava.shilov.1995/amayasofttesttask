﻿using UnityEngine;
using DG.Tweening;

public class CardController : MonoBehaviour
{
    [SerializeField]
    private SpriteRenderer _contentSpriteRenderer;

    [SerializeField]
    private GameObject _successParticles;

    private CardData _cardData;

    private Vector3 _initialScale;

    private Sequence _animationSequence;

    public CardData CardData {
        get {
            return _cardData;
        }
        set {
            _cardData = value;
            UpdateUI();
        }
    }

    private void Awake() {
        _initialScale = transform.localScale;
    }

    private void UpdateUI() {
        _contentSpriteRenderer.sprite = _cardData.Sprite;

        float contentRotation = _cardData.IsSpriteRotated ? -90 : 0;
        _contentSpriteRenderer.transform.rotation = Quaternion.Euler(0, 0, contentRotation);
    }

    public void DoSpawnAnimation() {
        _animationSequence?.Kill();

        _animationSequence = DOTween.Sequence();
        _animationSequence.Append(transform.DOScale(Vector3.zero, 0))
                .Append(transform.DOScale(_initialScale * 1.1f, 0.3f))
                .Append(transform.DOScale(_initialScale, 0.2f).SetLoops(2, LoopType.Yoyo));    
    }

    public void DoWrongAnimation() {
        Transform contentTransform = _contentSpriteRenderer.transform;
        float sectionTime = 0.2f;
        float amplitude = 0.05f;

        _animationSequence?.Kill();

        _animationSequence = DOTween.Sequence();
        _animationSequence.Append(contentTransform.DOLocalMoveX(-amplitude, sectionTime))
                .Append(contentTransform.DOLocalMoveX(amplitude, sectionTime * 2))
                .Append(contentTransform.DOLocalMoveX(0, sectionTime))
                .SetEase(Ease.InBounce);// Effect looks better without this modifier :/
    }

    public void DoSuccessAnimation(TweenCallback callback = null) {
        _animationSequence?.Kill();

        Instantiate(_successParticles, transform.position + Vector3.back, Quaternion.identity);

        Transform contentTransform = _contentSpriteRenderer.transform;
        Vector3 initialScale = contentTransform.localScale;

        _animationSequence = DOTween.Sequence();
        _animationSequence.Append(contentTransform.DOScale(initialScale * 0.9f, 0.1f))
                .Append(contentTransform.DOScale(initialScale * 1.1f, 0.2f))
                .Append(contentTransform.DOScale(initialScale, 0.2f).SetLoops(2, LoopType.Yoyo))
                .OnComplete(callback);
    }
}
