﻿using UnityEngine;

public class PlayGridController : MonoBehaviour
{
    [SerializeField]
    private GameObject _cardPrefab;

    [SerializeField]
    private CardBundleData[] _cardBundles;

    private CardGrid _cardGrid;

    private CardGridBuilder _cardGridBuilder;

    private void Awake() {
        if (_cardPrefab == null) {
            Debug.LogError("Fatal error: Card prefab not set.");
            AppHelper.Quit();
        }

        _cardGrid = null;

        _cardGridBuilder = new CardGridBuilder(_cardBundles);
    }

    public void FillGrid(LevelData levelData, bool showSpawnAnimation, out CardData objectiveCard) {
        if (_cardGrid != null) {
            _cardGrid.Clear();
        }

        _cardGrid = _cardGridBuilder.Build(levelData, _cardPrefab, out objectiveCard);

        foreach (GameObject card in _cardGrid.Data) {
            card.transform.SetParent(transform);
        }

        if (showSpawnAnimation) {
            ShowSpawnAnimation();
        }
    }

    private void ShowSpawnAnimation() {
        foreach (GameObject card in _cardGrid.Data) {
            card.GetComponent<CardController>().DoSpawnAnimation();
        }
    }

    public void ClearGrid() {
        _cardGrid.Clear();
    }
}
